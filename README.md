# Digital Fabrication 2024

Here you can find list of student repositories and links to their websites.

## DF I

1. x [repo](https://gitlab.com/AoA2810/digital-fabrication-alex/) [www](https://aoa2810.gitlab.io/digital-fabrication-alex/) Aleksandra Olegovna Artemenko
1. x [repo](https://gitlab.com/timeritualslabour/digital-fabrication) [www](https://timeritualslabour.gitlab.io/digital-fabrication/) Vytautas Bikauskas
1. x [repo](https://gitlab.com/l-lu-u/2024-digital-fabrication/) [www](https://l-lu-u.gitlab.io/2024-digital-fabrication/) Lu Chen
1. [repo]() [www]() Hongxi Du
1. x [repo](https://gitlab.com/Graham.Featherstone/digital-fabrication) [www](https://graham.featherstone.gitlab.io/digital-fabrication) Graham Anthony Featherstone
1. [repo]() [www]() Maikki Maaria Hartikainen
1. x [repo](https://gitlab.com/mikajarvi/digital-fabrication) [www](https://mikajarvi.gitlab.io/digital-fabrication/) Mika Olavi Järvi
1. x [repo](https://gitlab.com/miro.keimioniemi/digital-fabrication-portfolio) [www](https://digital-fabrication-portfolio-miro-keimioniemi-a2f2c11a6e705b8f.gitlab.io/) Miro Jesper Keimiöniemi
1. [repo]() [www]() Deborah Naomi Kumagai
1. x [repo](https://gitlab.com/Kunnamo/digital-fabrication) [www](https://kunnamo.gitlab.io/digital-fabrication/) Oiva Juhani Kunnamo
1. [repo]() [www]() Mengzhe Liao
1. [repo]() [www]() Mikko Aleksis Linko
1. [repo]() [www]() Mengqiao Liu
1. x [repo](https://gitlab.com/yhannahlou1/yh-digital-fabrication/) [www](https://yhannahlou1.gitlab.io/yh-digital-fabrication/) Yihan Lou
1. [repo]() [www]() Ramin Mahmoudi
1. x [repo](https://gitlab.com/zinaaaa/digital-fabrication) [www](https://zinaaaa.gitlab.io/digital-fabrication/) Zina Marpegan
1. x [repo](https://gitlab.com/TomiMonahan/digital-fabrication/) [www](https://tomimonahan.gitlab.io/digital-fabrication/) Tomi Jari Monahan
1. [repo]() [www]() Joan Nieto Puig
1. [repo]() [www]() Jue Ning
1. [repo]() [www]() Antti Juhani Nurminen
1. [repo]() [www]() Xiaojie Pi
1. x [repo](https://gitlab.com/veikkoraty/digital-fabrication) [www](https://veikkoraty.gitlab.io/digital-fabrication/) Veikko Lauri Iivari Räty
1. x [repo](https://gitlab.com/xuefeishi/df1) [www](https://xuefeishi.gitlab.io/df1/) Xuefei Shi
1. [repo]() [www]() Celeste Sanja Smareglia
1. [repo]() [www]() Viktor Andrej Teodosin
1. [repo]() [www]() Irmuun Tuguldur
1. x [repo](https://gitlab.com/swirkes/digital-fabrication) [www](https://swirkes.gitlab.io/digital-fabrication/) Shane Thomas Wirkes
1. [repo]() [www]() Chi Thanh Vo
1. [repo]() [www]() Rui Zeng

